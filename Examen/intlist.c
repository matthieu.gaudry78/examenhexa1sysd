#include<stdio.h>
#include<stdlib.h>

typedef struct node node;
struct node {
   int val;
   node *next;
    node *prev;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}
//la modification est dans ce append_val
node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);
    newnode->prev = NULL;
    
    if (head == NULL) { 
        head = newnode;
    } else {            
        walk = head;
        while (walk->next != NULL) { 
            walk = walk->next;
        }
        newnode->prev = walk;
        walk->next = newnode; 
    }
    return head;
}

void append_val2(node **phead, int val) {
    node *newnode, **walk;

    newnode = create_node(val);
    
    walk = phead;
    while (*walk) {
	    walk = &( (*walk)->next );
    }
    *walk = newnode;
}

int len(node *head)
{
    int counter = 0;
    while(head != NULL)
    {
        head = head->next;
        counter++;
    }
    return counter;
}

void forth_and_back(node *head)
{
    node *walk;
    node *avant;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        avant = walk;
        walk = walk->next;
    }

    //le bloc de code ci dessous nous permet de revenir 1 étape avant 
    //que walk ne devienne NULL et nous permet donc d'accéder au pointeur 
    //prev
    
    walk = avant;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->prev;
    }
    printf("\n");
    
}

int main() {
    node *head1 = NULL;
    node *head2 = NULL;
    node *entry;

    // on construit les listes 
    // avec les mêmes valeurs
    // avec l'une ou l'autre des fonctions
    // d'ajout, peu importe
    
    head1 = append_val(head1, 42);
    head1 = append_val(head1, 12);
    head1 = append_val(head1, 54);
    head1 = append_val(head1, 41);
    
    append_val2(&head2, 42);
    append_val2(&head2, 12);
    append_val2(&head2, 54);
    append_val2(&head2, 41);

    // contiennent-elles les même valeurs ?

    //print_list(head1);
    //print_list(head2);

    // votre code de test ici pour la fonction forth_and_back

    forth_and_back(head1);  

    
    
    return 0;

}
