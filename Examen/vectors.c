#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void add_vectors(int n, double *t1, double *t2, long double *t3)
{
    double difference;
    
    printf("T3 : ");

    for(int index = 0; index  != n; index++)
    {
        difference = (t1[index] - t2[index]);
        t3[index] = pow(difference, 2);
        printf("%Lf ", t3[index]);
    }
    printf("\n");
}

double norm_vector(int n, double *t)
{
    double sum = 0;

    for(int index = 0; index != n; index++)
    {
        sum = sum + pow(t[index], 2);
    }
    return sqrt(sum);
}

int main() {
	double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	long double T3[5];
	int n = 5;

    double euclide;

	printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\n");

    add_vectors(5, T1, T2, T3);
    euclide = norm_vector(5, T1);
    printf("%f\n", euclide);
	
	exit(EXIT_SUCCESS);
}
