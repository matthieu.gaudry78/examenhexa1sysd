#include<stdio.h>
#include<stdlib.h>

void calcul(float p1, float p2)
{
    float difference;
    float result;

    if(p1 > p2) 
    {
        difference = p1 - p2;
        result = (difference * 100) / p2;
        printf("diminution de %f pourcent \n", result);
    }
    else
    {
        difference = p2 - p1;
        result = (difference * 100) / p1;
        printf("augmentation de %f pourcent \n", result);
    }
}

int main()
{
    float prix1;
    float prix2;

    printf("entrez le premier prix : ");
    scanf("%f", &prix1);
    printf("entrez le deuxième prix : ");
    scanf("%f", &prix2);

    calcul(prix1, prix2);

    return 0;
}
