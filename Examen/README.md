1/ Quelles sont les étapes nécessaires pour supprimer un élément d'une
  liste chaînée en connaissant son adresse (un pointeur vers le
  nœud concerné) ?

On commence par passer en paramètre la liste et l'élément que l'on veut supprimer.
Puis on initialise 2 nodes un walk qui nous permet de naviguer dans la liste, donc il prendra comme valeur le head de notre liste qui correspond au premier élément.
Et un prev qui sera le walk un maillon avant c'est pourquoi on l'initialise à NULL.
On avance dans notre liste grace au walk jusqu'à ce que walk soit égale à notre élément que l'on souhaite supprimer.
A ce moment là notre prev est l'élément juste avant walk.
On vérifie que notre valeur de prev est non NULL(pour éviter toutes erreurs de segmentation)
Puis on donne à prev->next donc le pointeur vers le prochain noeud le prochain noeud à pointé soit le pointeur (next) du noeud à supprimer.
Puis on supprime le noeud à l'aide de la fonction ffree qui va désallouer de la memoire.

2/ Deux fonctions réalisent cette opération dans le fichier `linus.c`.
  Quelles sont les différences notables entre ces deux fonctions ?

Le type de retour. l'une ne renvoie rien l'autre renvoie un node.
Les arguments passer ne sont pas les memes d'un cote nous avons un pointeur de node et de l'autre un pointeur vers un pointeur de node.
 
3/ Pourquoi la seconde a besoin de renvoyer une valeur et l'autre non ?
  Quel est le rôle d'un pointeur vers un pointeur ici ?

la première a besoin de renvoyer une valeur car le head peut etre modifié alors que dans la deuxieme le head n'est pas modifié. Le pointeur de pointeur sert à éviter de devoir modifier le head. Le C part du principe que nous savons ce que nous fesons donc dans la première fonction le retour permet à l'utilisateur de manipuler son pointeur plus simplement.


4/ Pourquoi la seconde reçoit un pointeur vers un pointeur comme argument ?

Pour permettrer demodifier directement l'adresse du pointeur original et égaleme,t de rendre les modifications visibles dans la fonction appelante.

5/ En quelques mots, laquelle est la plus élégante selon vous ?

la deuxième car le c nous met à disposition des outils tel que les pointeurs et les adresses donc il est plus judicieux d'utiliser ces outils sinon l'utilisation du c n'eest pas justifier





