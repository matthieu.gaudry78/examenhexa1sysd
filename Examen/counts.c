#include<stdio.h>
#include<stdlib.h>

int count_char(char *string, char c)
{
    int index = 1;
    int counter = 0;    
    
    while(string[index] != '\0')
    {
        if(string[index] == c)
        {
            counter++;
        }
        index++;
    }
    return counter;
}

int count_words(char *string)  
{                            
    int counter = 1;
    int index = 0;

    while(string[index] != '\0')
    {
        if(string[index] == ' ')
        {
            counter++;
        }
        index++;
    }
    return counter;
}

int count_words_better(char *string)  
{                            
    int counter = 1;
    int index = 0;

    while(string[index] != '\0')
    {
        if(string[index] == ' ')
        {
            while(string[index] == ' ')
            {
                index++;
            }
            counter++;
        }
        index++;
    }
    return counter;
}

int main()
{
    char chaine[50];

    printf("entrez votre chaine de caractère (max 50) : ");
    scanf("%[^\n]", chaine);

    int counter = count_char(chaine, 'o');
    printf("il y a %d o dans votre chaine\n", counter);

    int space = count_words_better(chaine);
    printf("il y a %d mots dans votre chaine\n", space);

    return 0;
}
