#include<stdio.h>
#include<stdlib.h>

void small_to_zero(int *t, int n, int val)
{
    printf("Voici le nouveau tableau\n");

    for(int index = 0; index != n; index++)
    {
        if(t[index] <= val)
        {
            t[index] = 0;
        }
        printf("%d ", t[index]);
    }
    printf("\n");
}

int main()
{
    int tab[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };

    small_to_zero(tab, 11, 4);

    return 0;
    
}
